package dataDrivenTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Fetchingvaluethroughapachepoi {
	public static void main(String[] args) throws IOException {

		File f = new File("/home/shankar/Documents/TextData/Testdata2.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		int rows = sheet.getPhysicalNumberOfRows();
		int columns = sheet.getRow(0).getLastCellNum();
		for (int i = 0; i < rows; i++) {
			
			for (int j = 0; j < columns; j++) {
				String cellvalue = sheet.getRow(i).getCell(j).getStringCellValue();
				System.out.println(cellvalue);

			}
			System.out.println();
		}
		workbook.close();
		fis.close();
	} 
}
