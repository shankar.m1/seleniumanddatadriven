package dataDrivenTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;


public class passingMultipledataIntoScriptUsingJxl {

	public static void main(String[] args) throws BiffException, IOException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		File f = new File("/home/shankar/Documents/TextData/Testdata1.xls");
		FileInputStream fis = new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fis);
		Sheet sh = book.getSheet("Sheet1");
		 //String cellData = sh.getCell(1, 2).getContents();
		 //System.out.println("The value present is " + cellData);
		int rows = sh.getRows();
		int columns = sh.getColumns();

		for (int i = 1; i < rows; i++) {
			String username = sh.getCell(0, i).getContents();
			String password = sh.getCell(1, i).getContents();
			driver.findElement(By.linkText("Log in")).click();
			driver.findElement(By.id("Email")).sendKeys(username);
			driver.findElement(By.id("Password")).sendKeys(password);

			driver.findElement(By.xpath("//input[@value='Log in']")).click();

			driver.findElement(By.linkText("Log out")).click();

		}

	}

}
