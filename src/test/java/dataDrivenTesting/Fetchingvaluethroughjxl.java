package dataDrivenTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Fetchingvaluethroughjxl {

	public static void main(String[] args) throws BiffException, IOException {

		File f = new File("/home/shankar/Documents/TextData/Testdata1.xls");
		FileInputStream fis = new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fis);
		Sheet sh = book.getSheet("Sheet1");
		String cellData = sh.getCell(1, 2).getContents();
		System.out.println("The value present is " + cellData);

		int rows = sh.getRows();
		int columns = sh.getColumns();

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				cellData = sh.getCell(j, i).getContents();
				System.out.println(cellData);

			}

		}

	}

}
