package org.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Loginintodemowebshop {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//a[@class='ico-login']")).click();
		driver.findElement(By.id("Email")).sendKeys("shankarsampath1056@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("P@ssw0rd");
		driver.findElement(By.id("RememberMe")).click();
		driver.findElement(By.xpath("//input[@value='Log in']")).click();

		String title = driver.getTitle();
		System.out.println("The title of the page is " + title);
		driver.findElement(By.xpath("(//a[contains(text(),'Electronics')])[3]")).click();
		driver.findElement(By.xpath("(//a[contains(text(),'Cell')])[4]")).click();

		driver.findElement(By.partialLinkText("Smartphone")).click();
		driver.findElement(By.id("add-to-cart-button-43")).click();
		driver.findElement(By.partialLinkText("Shopping cart")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		driver.findElement(By.xpath("(//input[contains(text(),'')])[15]")).click();
		driver.findElement(By.id("PickUpInStore")).click();

		driver.findElement(By.xpath("(//input[contains(text(),'')])[29]")).click();
		driver.findElement(By.xpath("(//input[contains(text(),'')])[35]")).click();
		driver.findElement(By.xpath("(//input[contains(text(),'')])[36]")).click();
		driver.findElement(By.xpath("(//input[contains(text(),'')])[37]")).click();
        System.out.println("The code passed sucessfully");

		driver.close();
	}
}
